use image::codecs::png::PngEncoder;
use image::{imageops, ImageEncoder, Rgba, RgbaImage};
use pretty_qr::{PrettyQr, Shape};
use serde::Deserialize;
use std::error::Error;
use vercel_lambda::error::VercelError;
use vercel_lambda::http::StatusCode;
use vercel_lambda::{lambda, IntoResponse, Request, Response};

#[derive(Debug, Deserialize)]
struct Config {
    content: String,
}

fn handler(request: Request) -> Result<impl IntoResponse, VercelError> {
    let size = 500;
    let margin = 50;

    let body = request.body();
    let config: Config = serde_json::from_slice(&body).expect("failed to parse JSON config");

    let qr = PrettyQr::new(size)
        .shape(Shape::Circle)
        .anchor_shape(Shape::Circle)
        .connect_corners(true)
        .render(config.content.as_bytes());

    let mut out = RgbaImage::from_pixel(
        size + margin * 2,
        size + margin * 2,
        Rgba([255, 255, 255, 255]),
    );

    imageops::overlay(&mut out, &qr, margin, margin);

    let mut bytes = Vec::new();
    let encoder = PngEncoder::new(&mut bytes);
    encoder
        .write_image(&*out, out.width(), out.height(), image::ColorType::Rgba8)
        .expect("failed to write image");

    let response = Response::builder()
        .status(StatusCode::OK)
        .header("Content-Type", "image/png")
        .body(bytes)
        .expect("Internal Server Error");

    Ok(response)
}

fn main() -> Result<(), Box<dyn Error>> {
    Ok(lambda!(handler))
}
